#ifndef RELATION_H
#define RELATION_H

#include <ctime>
#include <string>

class Relation
{
public:
    Relation();
    bool operator==(const Relation &rhs);
    std::string sname;
    std::string name;
    int year;
    std::string type;
    bool built;
    int personId;
    int computerId;
};

#endif // RELATION_H
