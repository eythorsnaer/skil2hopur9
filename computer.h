#ifndef COMPUTER_H
#define COMPUTER_H

#include <ctime>
#include <string>

class Computer
{
public:
    Computer();
    bool operator==(const Computer &rhs);
    std::string name;
    int year;
    std::string type;
    bool built;

    int id;

    int computerId;

};

#endif // COMPUTER_H



