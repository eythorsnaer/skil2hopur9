#include "scientistrepository.h"
#include <iostream>

using namespace std;

ScientistRepository::ScientistRepository() {

    initializeDatabase();
}
ScientistRepository::~ScientistRepository() {

    db.close();
}

void ScientistRepository::initializeDatabase(){
    QString connectionName = "databaseConnect";

    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("skil2.sqlite");

        db.open();
    }
}

// Add scientist to database
void ScientistRepository::add(Scientist scientist) {

    initializeDatabase();
    QSqlQuery query(db);
    query.prepare("INSERT INTO Persons (Name, Born, Death, Gender)"
                  "VALUES (:name, :dateOfBirth, :dateOfDeath, :gender)");

    query.bindValue(":name", QString::fromStdString(scientist.name));
    query.bindValue(":dateOfBirth", QString::fromStdString(scientist.dateOfBirth));
    query.bindValue(":dateOfDeath", QString::fromStdString(scientist.dateOfDeath));
    query.bindValue(":gender", QString::fromStdString(scientist.gender));

    query.exec();
}

// List of Scientist and Computer relation selected from DB and added to memory
std::list<Scientist> ScientistRepository::list() {

    initializeDatabase();
    QSqlQuery query(db);
    query.exec("SELECT * FROM Persons");

    while (query.next()) {
         Scientist sl;
         sl.id =          query.value("PersonID").toString().toInt();         //fáum qstring til baka úr toString þurfum þess vegna að gera toStdString
         sl.name =        query.value("Name").toString().toStdString();
         sl.dateOfBirth = query.value("Born").toString().toStdString();
         sl.dateOfDeath = query.value("Death").toString().toStdString();
         sl.gender =      query.value("Gender").toString().toStdString();
         scientistList.push_back(sl);
    }
    return scientistList;
}

// List of scientists selected from datadase in requested order and added to memory to display
std::list<Scientist> ScientistRepository::list(std::string col, std::string mod) {

    std::list<Scientist> outlist; // = std::list<Scientist>();

    initializeDatabase();
    QSqlQuery query(db);
    if(col == "1" && mod == "+") {
        std::cout << endl << "Names ascending\n" << endl;
        query.exec("SELECT * FROM Persons p ORDER by p.Name asc");
    } else if(col == "1" && mod == "-") {
        std::cout << endl << "Names descending\n" << endl;
        query.exec("SELECT * FROM Persons p ORDER BY p.Name desc");
    } else if(col == "2" && mod == "+") {
        std::cout << endl << "Birthyear ascending\n" << endl;
        query.exec("SELECT * FROM Persons p ORDER BY p.Born asc");
    } else if(col == "2" && mod == "-") {
        std::cout << endl << "Birthyear descending\n" << endl;
        query.exec("SELECT * FROM Persons p ORDER BY p.Born desc");
    } else if(col == "3" && mod == "+") {
        std::cout << endl << "Year of death ascending\n" << endl;
        query.exec("SELECT * FROM Persons p ORDER BY p.Death asc");
    } else if(col == "3" && mod == "-") {
        std::cout << endl << "Year of death descending\n" << endl;
        query.exec("SELECT * FROM Persons p ORDER BY p.Death desc");
    } else if(col == "4" && mod == "+") {
        std::cout << endl << "By gender ascending\n" << endl;
        query.exec("SELECT * FROM Persons p ORDER BY p.Gender asc");
    } else if(col == "4" && mod == "-") {
        std::cout << endl << "By gender descending\n" << endl;
        query.exec("SELECT * FROM Persons p ORDER BY p.Gender desc");
    } else if(col == "5" && mod == "+") {
        std::cout << endl << "ID ascending\n" << endl;
        query.exec("SELECT * FROM Persons p ORDER BY p.PersonID asc");
    } else if(col == "5" && mod == "-") {
        std::cout << endl << "ID descending\n" << endl;
        query.exec("SELECT * FROM Persons p ORDER BY p.PersonID desc");
    }
     else {
        throw std::runtime_error(col + " or " + mod + " is not a legal filter.");
    }

    while (query.next()) {
         Scientist sl;
         sl.id =          query.value("PersonID").toString().toInt();
         sl.name =        query.value("Name").toString().toStdString();     //fáum qstring til baka úr toString þurfum þess vegna að gera toStdString
         sl.dateOfBirth = query.value("Born").toString().toStdString();
         sl.dateOfDeath = query.value("Death").toString().toStdString();
         sl.gender =      query.value("Gender").toString().toStdString();

         outlist.push_back(sl);
    }
    return outlist;
}
// search of scientists selected from datadase and added to memory
std::list<Scientist>  ScientistRepository::search(std::string searchTerm) {

    Scientist s;

    std::list<Scientist> searchList = std::list<Scientist>();
    initializeDatabase();
    QSqlQuery query(db);
    query.prepare("SELECT * FROM Persons p WHERE p.Name LIKE '%'|| ? || '%'");
    query.addBindValue(QString::fromStdString(searchTerm));
    query.exec();

    while(query.next()) {
        s.id =        query.value("PersonID").toString().toInt();
        s.name         = query.value("Name").toString().toStdString();
        s.dateOfBirth  = query.value("Born").toString().toStdString();
        s.dateOfDeath  = query.value("Death").toString().toStdString();
        s.gender       = query.value("Gender").toString().toStdString();
        searchList.push_back(s);
     }
    return searchList;
}
