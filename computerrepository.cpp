#include "computerrepository.h"
#include <iostream>

using namespace std;

ComputerRepository::ComputerRepository() {
    initializeDatabase();
}
ComputerRepository::~ComputerRepository() {
    db.close();
}
void ComputerRepository::initializeDatabase(){
    QString connectionName = "databaseConnect";
    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("skil2.sqlite");
        db.open();
    }
}
// Add computer to databade
void ComputerRepository::add(Computer computer) {

    initializeDatabase();
    QSqlQuery query(db);
    query.prepare("INSERT INTO Computers (ComputerName, InventionYear, TypeId, Built)"
                  "VALUES (:name, :Year, :Type, :Built)");
    query.bindValue(":name", QString::fromStdString(computer.name));
    query.bindValue(":Year", QString::number(computer.year));
    query.bindValue(":Type", QString::fromStdString(computer.type));
    query.bindValue(":Built", QString::number(computer.built));

    query.exec();
}

// select computers from database in requested order and the resault to memory
std::list<Computer> ComputerRepository::listComp(std::string col, std::string mod) {

    std::list<Computer> outlist;// = std::list<Computer>();
    initializeDatabase();
    QSqlQuery query(db);
    if(col == "1" && mod == "+") {
        std::cout << endl << "Names ascending\n" << endl;
        query.exec("SELECT c.ComputerID,c.ComputerName,c.InventionYear,c.Built, CT.type FROM Computers c "
                   "left join Computertypes CT ON CT.typeId = c.typeId "
                   "ORDER by c.ComputerName asc");
    } else if(col == "1" && mod == "-") {
        std::cout << endl << "Names descending\n" << endl;
        query.exec("SELECT c.ComputerID,c.ComputerName,c.InventionYear,c.Built, CT.type FROM Computers c "
                   "left join Computertypes CT ON CT.typeId = c.typeId "
                   "ORDER BY c.ComputerName desc");
    } else if(col == "2" && mod == "+") {
        std::cout << endl << "Invention year ascending\n" << endl;
        query.exec("SELECT c.ComputerID,c.ComputerName,c.InventionYear,c.Built, CT.type FROM Computers c "
                   "left join Computertypes CT ON CT.typeId = c.typeId "
                   "ORDER BY c.InventionYear asc");
    } else if(col == "2" && mod == "-") {
        std::cout << endl << "Invention year descending\n" << endl;
        query.exec("SELECT c.ComputerID,c.ComputerName,c.InventionYear,c.Built, CT.type FROM Computers c "
                   "left join Computertypes CT ON CT.typeId = c.typeId "
                   "ORDER BY c.InventionYear desc");
    } else if(col == "3" && mod == "+") {
        std::cout << endl << "Type ascending\n" << endl;
        query.exec("SELECT c.ComputerID,c.ComputerName,c.InventionYear,c.Built, CT.type FROM Computers c "
                   "left join Computertypes CT ON CT.typeId = c.typeId "
                   "ORDER BY CT.Type asc");
    } else if(col == "3" && mod == "-") {
        std::cout << endl << "Type descending\n" << endl;
        query.exec("SELECT c.ComputerID,c.ComputerName,c.InventionYear,c.Built, CT.type FROM Computers c "
                   "left join Computertypes CT ON CT.typeId = c.typeId "
                   "ORDER BY CT.Type desc");
    } else if(col == "4" && mod == "+") {
        std::cout << endl << "Built or not ascending\n" << endl;
        query.exec("SELECT c.ComputerID,c.ComputerName,c.InventionYear,c.Built, CT.type FROM Computers c "
                   "left join Computertypes CT ON CT.typeId = c.typeId "
                   "ORDER BY c.Built asc");
    } else if(col == "4" && mod == "-") {
        std::cout << endl << "Built or not descending\n" << endl;
        query.exec("SELECT c.ComputerID,c.ComputerName,c.InventionYear,c.Built, CT.type FROM Computers c "
                   "left join Computertypes CT ON CT.typeId = c.typeId "
                   "ORDER BY c.Built desc");
    } else if(col == "5" && mod == "+") {
        std::cout << endl << "ID ascending\n" << endl;
        query.exec("SELECT c.ComputerID,c.ComputerName,c.InventionYear,c.Built, CT.type FROM Computers c "
                   "left join Computertypes CT ON CT.typeId = c.typeId "
                   "ORDER BY c.ComputerID asc");
    } else if(col == "5" && mod == "-") {
        std::cout << endl << "ID descending\n" << endl;
        query.exec("SELECT c.ComputerID,c.ComputerName,c.InventionYear,c.Built, CT.type FROM Computers c "
                   "left join Computertypes CT ON CT.typeId = c.typeId "
                   "ORDER BY c.ComputerID desc");
    }
     else {
        throw std::runtime_error(col + " or " + mod + " is not a legal filter.");
    }
    while (query.next()) {
         Computer c;
         c.id    = query.value("ComputerID").toString().toInt();
         c.name  = query.value("ComputerName").toString().toStdString();     //fáum qstring til baka úr toString þurfum þess vegna að gera toStdString
         c.type  = query.value("type").toString().toStdString();
         c.year  = query.value("InventionYear").toString().toInt();
         c.built = query.value("Built").toString().toInt();
         outlist.push_back(c);
    }
    return outlist;
}
// searh for computers in database and adds the resault to memory
std::list<Computer>  ComputerRepository::searchCom(std::string searchTerm) {

    Computer com;
    std::list<Computer> searchComputer = std::list<Computer>();
    initializeDatabase();
    QSqlQuery query(db);
    query.prepare("SELECT c.ComputerID,c.ComputerName,c.InventionYear,c.Built, CT.type FROM Computers c "
                  "left join Computertypes CT ON CT.typeId = c.typeId "
                  "WHERE c.ComputerName LIKE '%'|| ? || '%'");
    query.addBindValue(QString::fromStdString(searchTerm));
    query.exec();

    while(query.next()) {
        com.id    = query.value("ComputerID").toString().toInt();
        com.name  = query.value("ComputerName").toString().toStdString();     //fáum qstring til baka úr toString þurfum þess vegna að gera toStdString
        com.type  = query.value("Type").toString().toStdString();
        com.year  = query.value("InventionYear").toString().toInt();
        com.built = query.value("Built").toString().toInt();
        searchComputer.push_back(com);
     }
    return searchComputer;
}
