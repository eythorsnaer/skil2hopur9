#-------------------------------------------------
#
# Project created by QtCreator 2014-12-03T20:38:43
#
#-------------------------------------------------

QT       += core sql

QT       -= gui

TARGET = Solution1
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    consoleui.cpp \
    relation.cpp  \
    scienceservice.cpp \
    scientist.cpp \
    scientistrepository.cpp \
    computer.cpp \
    computerrepository.cpp \
    relationrepository.cpp

HEADERS += \
    consoleui.h \
    menus.h \
    relation.h  \
    scienceservice.h \
    scientist.h \
    scientistrepository.h \
    computer.h \
    computerrepository.h \
    relationrepository.h
