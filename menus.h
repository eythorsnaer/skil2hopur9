#ifndef MENUS_H
#define MENUS_H

// Long strings and ASCII art here below \/ \/

std::string MAIN_MENU = "Please use the following commands to interface with this AWESOME DATABASE\n\n\
|     1: Add a record to the database                               |\n\
|     2: Order and display the records in the database              |\n\
|     3: Searches for a scientist or a computer                     |\n\
|     q: Exit the program;                                          |";

std::string FILTER_MENU = "Enter one of the following commands to order by the corresponding column\n\
along with an ascending(+) or descending(-) modifier\n\
|     1: Name               |\n\
|     2: Date of birth      |\n\
|     3: Date of death      |\n\
|     4: Gender             |\n\
|     5: ID                 |\n\n\
Example: \"1 +\"";

std::string FILTER_MENU_COMP = "Enter one of the following commands to order by the corresponding column\n\
along with an ascending(+) or descending(-) modifier\n\
|     1: Name                           |\n\
|     2: Year of invention              |\n\
|     3: Type of computer               |\n\
|     4: Was it built (1) or not (0)    |\n\
|     5: ID                             |\n\n\
Example: \"1 +\"";

std::string FILTER_MENU_RELATION = "Enter one of the following commands to order by the corresponding column\n\
along with an ascending(+) or descending(-) modifier\n\
|     1: Scientist name                 |\n\
|     2: Computer name                  |\n\
Example: \"1 +\"";

std::string ADD_MENU = "Enter what you want to add to this AWESOME DATABASE\n\n\
|     1: Scientist record                           |\n\
|     2: Computer information                       |\n\
|     3: Relation between sientist and computer     |";

std::string ORDER_MENU = "Enter what you want to sort\n\n\
|     1: Scientist record      |\n\
|     2: Computer information  |\n\
|     3: Relation information  |";

std::string SEARCH_MENU = "Enter what you want to sort\n\n\
|     1: Search for scientists                       |\n\
|     2: Search for computers                        |\n\
|     3: Search for scientists related to computer   |\n\
|     4: Search for computer related to scientists   |";

std::string COW_SAYS ="\
                    \\   ^__^\n\
                     \\  (oo)\\_______\n\
                        (__)\\       )\\/\\ \n\
                            ||----w | \n\
                            ||     ||\n";

#endif // MENUS_H
