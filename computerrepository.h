#ifndef COMPUTERREPOSITORY_H
#define COMPUTERREPOSITORY_H

#include <list>
#include "Scientist.h"
#include "computer.h"
#include "relation.h"
#include <stdexcept>
#include <string>
#include <algorithm>
#include <QtSql>

using namespace std;

// Handles all the things that pertain to persistence
// uses a file that is located in the build root directory
// for persistent storage
class ComputerRepository {
public:
    ComputerRepository();
    ~ComputerRepository();
    void add(Computer);
    std::list<Computer> listCom();
    std::list<Computer> searchCom(std::string searchTerm);
    std::list<Computer> listComp(std::string col, std::string mod);  
    void initializeDatabase();
private:
    // This list is maintained in memory and persisted with ScientistRepository::save()
    std::list<Computer> ComputerList;
    // The filename of the file that is used to persist data
    std::string filename;
    // The character that delimits each column of a line
    char delimiter;
    // Persist the private list to a file
     QSqlDatabase db;
    void save();
};

#endif // COMPUTERREPOSITORY_H
