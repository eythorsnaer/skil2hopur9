#include "scienceservice.h"

ScienceService::ScienceService() {
    scientistRepository = ScientistRepository();
     computerRepository = ComputerRepository();
     relationRepository = RelationRepository();
}

ScienceService::~ScienceService() {
}

void ScienceService::addScientist(Scientist scientist) {
    scientistRepository.add(scientist);
}

void ScienceService::addComputer(Computer computer) {
    computerRepository.add(computer);
}

void ScienceService::addRelation(Relation relation) {
    relationRepository.add(relation);
}

list<Scientist> ScienceService::getScientistsOrderedBy(string col, string mod) {
    return scientistRepository.list(col,mod);
}

list<Computer> ScienceService::getComputersOrderedBy(string col, string mod) {
    return computerRepository.listComp(col,mod);
}
list<Relation> ScienceService::getRelationsOrderedBy(string col, string mod) {
    return relationRepository.list(col,mod);
}
list<Scientist> ScienceService::search(string searchTerm) {
    return scientistRepository.search(searchTerm);
}

list<Computer> ScienceService::searchCom(string searchTerm) {
    return computerRepository.searchCom(searchTerm);
}

list<Relation> ScienceService::searchRelation(string searchTerm) {
    return relationRepository.search(searchTerm);
}

list<Relation> ScienceService::searchRelationCom(string searchTerm) {
    return relationRepository.searchcom(searchTerm);
}

