#include "relationrepository.h"

RelationRepository::RelationRepository()
{
    initializeDatabase();
}

RelationRepository::~RelationRepository() {
    db.close();
}

void RelationRepository::initializeDatabase(){
    QString connectionName = "databaseConnect";

    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("skil2.sqlite");

        db.open();
    }
}
// Add relations to database
void RelationRepository::add(Relation relation) {

    initializeDatabase();
    QSqlQuery query(db);
    query.prepare("INSERT INTO PersCompRelation (PersonId, ComputerId)"
                  "VALUES (:PersonId, :ComputerId)");

    query.bindValue(":PersonId", QString::number(relation.personId));
    query.bindValue(":ComputerId", QString::number(relation.computerId));

    query.exec();
}

// List of Scientist and Computer relation selected from DB and added to memory

std::list<Relation> RelationRepository::list(std::string col, std::string mod) {

    std::list<Relation> outlist;
    initializeDatabase();
    QSqlQuery query(db);
    if(col == "1" && mod == "+") {
        std::cout << endl << "Names ascending\n" << endl;
        query.exec("select * from persons p "
                   "left join PersCompRelation cmp on p.personId = cmp.PersonId "
                   "left join Computers C on cmp.ComputerId = C.ComputerId "
                   "left join computerTypes CT ON c.typeid = CT.typeId "
                   "ORDER BY Name");
    } else if(col == "1" && mod == "-") {
        std::cout << endl << "Names descending\n" << endl;
        query.exec("select * from persons p "
                   "left join PersCompRelation cmp on p.personId = cmp.PersonId "
                   "left join Computers C on cmp.ComputerId = C.ComputerId "
                   "left join computerTypes CT ON c.typeid = CT.typeId "
                   "ORDER BY Name desc");
    } else if(col == "2" && mod == "+") {
        std::cout << endl << "Computername ascending\n" << endl;
        query.exec("select * from persons p "
                   "left join PersCompRelation cmp on p.personId = cmp.PersonId "
                   "left join Computers C on cmp.ComputerId = C.ComputerId "
                   "left join computerTypes CT ON c.typeid = CT.typeId "
                   "ORDER BY computerName");
    } else if(col == "2" && mod == "-") {
        std::cout << endl << "Computername descending\n" << endl;
        query.exec("select p.PersonID,p.Name,C.ComputerName,CT.Type,C.InventionYear,C.Built from persons p "
                   "left join PersCompRelation cmp on p.personId = cmp.PersonId "
                   "left join Computers C on cmp.ComputerId = C.ComputerId "
                   "left join computerTypes CT ON c.typeid = CT.typeId "
                   "ORDER BY computerName desc");
    }
    while (query.next()) {
         Relation rel;
         rel.personId = query.value("PersonID").toString().toInt();
         rel.sname = query.value("Name").toString().toStdString();
         rel.name = query.value("ComputerName").toString().toStdString();     //fáum qstring til baka úr toString þurfum þess vegna að gera toStdString
         rel.type = query.value("Type").toString().toStdString();
         rel.year = query.value("InventionYear").toString().toInt();
         rel.built =query.value("Built").toString().toInt();

         outlist.push_back(rel);
    }
    return outlist;
}

// search of scientists theyre relation to computers selected from datadase and added to memory
std::list<Relation>  RelationRepository::search(std::string searchTerm) {

    Relation r;

    std::list<Relation> searchList = std::list<Relation>();
    initializeDatabase();
    QSqlQuery query(db);
    query.prepare("select p.Name,C.ComputerName,CT.Type,C.InventionYear,C.Built from persons p "
                  "inner join PersCompRelation cmp on p.personId = cmp.PersonId "
                  "inner join Computers C on cmp.ComputerId = C.ComputerId "
                  "left join computerTypes CT ON c.typeid = CT.typeId "
                  "WHERE p.Name LIKE '%'|| ? || '%' ");
    query.addBindValue(QString::fromStdString(searchTerm));
    query.exec();

    while(query.next()) {
        r.sname        = query.value("Name").toString().toStdString();
        r.name         = query.value("ComputerName").toString().toStdString();
        r.type         = query.value("Type").toString().toStdString();
        r.year         = query.value("InventionYear").toString().toInt();
        r.built        = query.value("Built").toString().toInt();
        searchList.push_back(r);
     }
    db.close();
    return searchList;
}

// search of computers theyre relation to scientists selected from datadase and added to memory
std::list<Relation>  RelationRepository::searchcom(std::string searchTerm) {

    Relation r;

    std::list<Relation> searchList = std::list<Relation>();
    initializeDatabase();
    QSqlQuery query(db);
    query.prepare("select p.Name,C.ComputerName,CT.Type,C.InventionYear,C.Built from persons p "
                  "inner join PersCompRelation cmp on p.personId = cmp.PersonId "
                  "inner join Computers C on cmp.ComputerId = C.ComputerId "
                  "left join computerTypes CT ON c.typeid = CT.typeId "
                  "WHERE c.ComputerName LIKE '%'|| ? || '%' ");
    query.addBindValue(QString::fromStdString(searchTerm));
    query.exec();

    while(query.next()) {
        r.name         = query.value("ComputerName").toString().toStdString();
        r.sname        = query.value("Name").toString().toStdString();
        r.type         = query.value("Type").toString().toStdString();
        r.year         = query.value("InventionYear").toString().toInt();
        r.built        = query.value("Built").toString().toInt();
        searchList.push_back(r);
     }
    db.close();
    return searchList;
}
