#ifndef CONSOLEUI_H
#define CONSOLEUI_H

#include "Scientist.h"
#include <stack>
#include <list>
#include "scienceservice.h"
#include  "computer.h"
#include <fstream>

using  namespace std;

// Display layer for the console application
class ConsoleUI {
public:
    ConsoleUI();
    ~ConsoleUI();
    int start();
    ScienceService scienceService;
    void print(list<Scientist> printList);
    void print(list<Computer> printList);
    void print(list<Relation> printList);
    void addScientist();
    void addComputer();
    void addRelation();
    void handleSearch();
    void handleOrder();
    void addMenu();
    void banner();
private:
    void clear();
    void waitForPrompt(int a);
    int respondToMessage();
};

#endif // CONSOLEUI_H
