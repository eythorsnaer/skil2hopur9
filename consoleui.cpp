#include "consoleui.h"
#include <iostream>
#include <string>
#include "menus.h"
#include <stdexcept>
#include <sstream>

ConsoleUI::ConsoleUI() {
    scienceService = ScienceService();
}
ConsoleUI::~ConsoleUI() {
}
int ConsoleUI::start() {
    // REPL
    while(true) {
        int response = respondToMessage();
        if( response == 0)
        {
            return 0;
        }
    }
    return 1;
}

void ConsoleUI::clear() {
    // Not pretty, but it is platform independant
    cout << string( 100, '\n' );
}
void ConsoleUI::banner(){
    string line;
    ifstream myfile ("banner2.txt");
    cout << endl;
    if (myfile.is_open())
    {
      while ( getline (myfile,line) )
      {
        cout << line << '\n';
      }
      myfile.close();
    }
    cout << endl << endl;
}
void ConsoleUI::waitForPrompt(int a) {                  // int a cin.ignore Voodoo.
    cout << "\nPress enter to continue..." << endl;
    cin.ignore(a);
    cin.get();
}
void ConsoleUI::print(list<Scientist> printList){
    int c = 1;
    cout << "\tName:\t\t\tDateOfBirth:\tDateOfDeath:\tGender:\n";
    for(list<Scientist>::iterator iter = printList.begin(); iter != printList.end(); iter ++) {
        cout << "ID: " <<  iter->id << "\t";
        cout.width(24);
        cout << left << iter->name;
        cout.width(16);
        cout << left << iter->dateOfBirth;
        cout.width(16);
        cout << left << iter->dateOfDeath;
        cout.width(10);
        if(iter->gender == "m")
            cout << "Male" << endl;
        else if (iter->gender == "f")
            cout << "Female" << endl;
        else
        cout << left << iter->gender << endl;
        c++;
    }
}
void ConsoleUI::print(list<Computer> printList){
    int c = 1;
    cout << "\tName:\t\t\tType:\t\tYear:\tBuilt:\n";
    for(list<Computer>::iterator iter = printList.begin(); iter != printList.end(); iter ++) {
        cout << "ID: " <<  iter->id << "\t";
        cout.width(24);
        cout << left << iter->name;
        cout.width(16);
        cout << left << iter->type;
        cout.width(8);
        cout << left << iter->year;
        cout.width(8);
        if(iter->built == 1){
            cout << "Yes"<< endl;
        }
        else if (iter->built== 0)
            cout << "No" << endl;
        c++;
    }
}
void ConsoleUI::print(list<Relation> printList){
    cout << "Name:\t\t\tComputer:\t\tType:\t\tYear:\tBuilt:\n";
    for(list<Relation>::iterator iter = printList.begin(); iter != printList.end(); iter ++) {
        if(iter->name != ""){
            cout.width(24);
            cout << left << iter->sname;
            cout.width(24);
            cout << left << iter->name;
            cout.width(16);
            cout << left << iter->type;
            cout.width(8);
            cout << left << iter->year;
            cout.width(6);
            if(iter->built == 1){
                cout << "Yes"<< endl;
            }
            else if (iter->built== 0)
                cout << "No" << endl;
        }
    }
}
int ConsoleUI::respondToMessage() {

    banner();
    cout << MAIN_MENU << endl;
    string userRequest;
    cin >> userRequest;
    try {
        // Handle all available commands and throw error on unknown ones
        if(userRequest.find("1") != string::npos) {
            addMenu();
        }
        else if(userRequest.find("2") != string::npos) {
                handleOrder();
        }
        else if(userRequest.find("3") != string::npos) {
               handleSearch();
        }
         else if (userRequest.find("q") != string::npos) {
            return 0;
         }
         else{
            throw runtime_error( userRequest + " is not a valid command.");
         }
    }catch(runtime_error e) {
        clear();
        cout << "Command caused an error: " << e.what() << endl;
        cout << "Please try another command" << endl;
        waitForPrompt(1);
        clear();
    }
    return 1;
}
void ConsoleUI::addScientist()
{
    Scientist additionalScientist = Scientist();
    clear();
    string name =  "\
            ------------------------------------\n\
           < Enter the name of the scientist!!! >\n\
            ------------------------------------\n";
    cout << name << COW_SAYS;
    cin.ignore();
    getline(cin, additionalScientist.name);
    clear();
    string birth = "\
            ---------------------------------------------\n\
           < Enter the date of birth of the scientist!!! >\n\
            ---------------------------------------------\n";
    cout << birth << COW_SAYS;
    cin >> additionalScientist.dateOfBirth;
    clear();
    string death =  "\
            ---------------------------------------------\n\
           < Enter the date of death of the scientist!!!  >\n\
            ---------------------------------------------\n";
    cout << death << COW_SAYS;
    cin >> additionalScientist.dateOfDeath;
    clear();
    string gender ="\
            --------------------------------------------\n\
           < Enter the gender of the scientist!!! (m/f) >\n\
            --------------------------------------------\n";
    cout << gender << COW_SAYS;
    cin >> additionalScientist.gender;
    if (additionalScientist.gender != "m" || additionalScientist.gender != "f")
    scienceService.addScientist(additionalScientist);
    clear();
}
void ConsoleUI::addComputer()
{
    Computer additionalComputer = Computer();
    cout << "Enter the name of the computer: ";
    cin.ignore();
    getline(cin, additionalComputer.name);
    clear();
    cout << "Enter the year of the invention of the computer: ";
    cin >> additionalComputer.year;
    clear();
    cout << "Enter number of the type of the computer: \n"<<
            "1 for mechanical, 2 for electronic , 3 for transistor ";
    cin >> additionalComputer.type;
    clear();
    cout << "Enter \"1\" for if it was built or \"0\"if not: ";
    cin >> additionalComputer.built;
    scienceService.addComputer(additionalComputer);
    clear();
}

void ConsoleUI::addRelation()
{
    Relation additionalRelation = Relation();
    clear();
    std::list<Computer> lc = scienceService.getComputersOrderedBy("5", "+");
    print(lc);                                                              //prints out all computers in the database
    cout << "Enter the ID of the computer: ";
    cin.ignore();
    cin >> additionalRelation.computerId;
    clear();
    std::list<Scientist> ls = scienceService.getScientistsOrderedBy("1","+");       //prints out all scientists in the database
    print(ls);
    cout << "Enter the ID of the scientist: ";
    cin >> additionalRelation.personId;
    clear();
    scienceService.addRelation(additionalRelation);
    clear();
}

void ConsoleUI::handleSearch()
{
    clear();
    cout << SEARCH_MENU << endl;
    string userRequest;
    cin >> userRequest;
    if (userRequest == "1"){
        clear();
        string searchTerm = "";
        cout << "Enter the search term: ";
        cin.ignore();
        getline(cin,searchTerm);
        clear();
        list<Scientist> searchResult = scienceService.search(searchTerm);
        if(searchResult.size() > 0) {
            cout << "Scientist(s) found!!" << endl;
            print(searchResult);

            waitForPrompt(0);
            clear();
        }
        else if(searchResult.size() == 0) {
            cout << "No scientist found!!" << endl;
            waitForPrompt(0);
            clear();
        }
        else{
            cout << "No results found for the term: " << searchTerm << endl;
                waitForPrompt(0);
                clear();
        }
    }
    else if(userRequest == "2"){
        clear();
        string searchTerm = "";
        cout << "Enter the search term: ";
        cin.ignore();
        getline(cin,searchTerm);
        clear();
        list<Computer> searchResultComputer = scienceService.searchCom(searchTerm);
        if(searchResultComputer.size() > 0) {
            cout << "Computer found!!" << endl;
            print(searchResultComputer);
            waitForPrompt(0);
            clear();
    }
        else if(searchResultComputer.size() == 0) {
            cout << "No computer found!!" << endl;
            waitForPrompt(0);
            clear();
        }
        else{
            cout << "No results found for the term: " << searchTerm << endl;
                waitForPrompt(0);
                clear();
        }
    }
    else if(userRequest == "3"){
        clear();
        string searchTerm = "";
        cout << "Enter the search term: ";
        cin.ignore();
        getline(cin,searchTerm);
        clear();
        list<Relation> searchResultRelation = scienceService.searchRelation(searchTerm);
        if(searchResultRelation.size() > 0) {
            cout << "Relation found!!" << endl;
            print(searchResultRelation);
            waitForPrompt(0);
            clear();
    }
        else if(searchResultRelation.size() == 0) {
            cout << "No Relation found!!" << endl;
            waitForPrompt(0);
            clear();
        }
        else{
            cout << "No results found for the term: " << searchTerm << endl;
                waitForPrompt(0);
                clear();
        }
    }

    else if(userRequest == "4"){
        clear();
        string searchTerm = "";
        cout << "Enter the search term: ";
        cin.ignore();
        getline(cin,searchTerm);
        clear();
        list<Relation> searchResultRelation = scienceService.searchRelationCom(searchTerm);
        if(searchResultRelation.size() > 0) {
            cout << "Relation found!!" << endl;
            print(searchResultRelation);
            waitForPrompt(0);
            clear();
    }
        else if(searchResultRelation.size() == 0) {
            cout << "No Relation found!!" << endl;
            waitForPrompt(0);
            clear();
        }
        else{
            cout << "No results found for the term: " << searchTerm << endl;
                waitForPrompt(0);
                clear();
        }
    }

}
void ConsoleUI::handleOrder()
{
    clear();
    cout << ORDER_MENU << endl;
    string addUserRequest;
    cin>>addUserRequest;
    clear();
    string filterCol = "";
    string filterMod = "+";
    if (addUserRequest == "1"){
        cout << FILTER_MENU << endl;
        cin >> filterCol >> filterMod;
        clear();
        list<Scientist> l = scienceService.getScientistsOrderedBy(filterCol,filterMod);
        print(l);
        waitForPrompt(1);
        clear();
    }
    else if (addUserRequest == "2"){
        cout << FILTER_MENU_COMP << endl;
        cin >> filterCol >> filterMod;
        clear();
        list<Computer> l = scienceService.getComputersOrderedBy(filterCol,filterMod);
        print(l);
        waitForPrompt(1);
        clear();
    }
    else if (addUserRequest == "3"){
        cout << FILTER_MENU_RELATION << endl;
        cin >> filterCol >> filterMod;
        clear();
        list<Relation> l = scienceService.getRelationsOrderedBy(filterCol,filterMod);
        print(l);
        waitForPrompt(1);
        clear();
    }
    else {
        cout << addUserRequest << " is not a valid choice" << endl;
        waitForPrompt(1);
        clear();
    }
}
void ConsoleUI::addMenu()
{
    clear();
    cout << ADD_MENU << endl;
    string addUserRequest;
    cin >> addUserRequest;
    if (addUserRequest == "1"){
        addScientist();
    }
    else if (addUserRequest == "2"){
        addComputer();
    }
    else if (addUserRequest == "3"){
        addRelation();
    }
}

